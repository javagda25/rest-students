package com.javagda25.restapi.service;

import com.javagda25.restapi.exception.WrongOperation;
import com.javagda25.restapi.mapper.GradeMapper;
import com.javagda25.restapi.mapper.StudentMapper;
import com.javagda25.restapi.model.Grade;
import com.javagda25.restapi.model.Student;
import com.javagda25.restapi.model.dto.AddGradeToStudent;
import com.javagda25.restapi.model.dto.AssignGradeToStudent;
import com.javagda25.restapi.model.dto.CreateStudentRequest;
import com.javagda25.restapi.model.dto.StudentUpdateRequest;
import com.javagda25.restapi.repository.GradeRepository;
import com.javagda25.restapi.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentMapper studentMapper;
    @Autowired
    private GradeMapper gradeMapper;

    @Autowired
    private GradeRepository gradeRepository;

    public List<Student> getAll() {
        return studentRepository.findAll();
    }

    public Student getById(Long studentId) {
        Optional<Student> studentOptional = studentRepository.findById(studentId);
        if (studentOptional.isPresent()) {
            return studentOptional.get();
        }
        throw new EntityNotFoundException("student, id:" + studentId);
    }

    public Long save(CreateStudentRequest dto) {
        Student student = studentMapper.createStudentFromDto(dto);

        /*pobierz oceny, następnie ustaw w studencie i zapisz nowego do bazy*/

        return studentRepository.save(student).getId();
    }

    public void update(StudentUpdateRequest studentToEdit) {
        Optional<Student> studentOptional = studentRepository.findById(studentToEdit.getStudentId());
        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();

            if (studentToEdit.getName() != null) {
                student.setName(studentToEdit.getName());
            }
            if (studentToEdit.getSurname() != null) {
                student.setSurname(studentToEdit.getSurname());
            }
            if (studentToEdit.getDateOfBirth() != null) {
                student.setDateOfBirth(studentToEdit.getDateOfBirth());
            }
            if (studentToEdit.getIsAlive() != null) {
                student.setAlive(studentToEdit.getIsAlive());
            }

            studentRepository.save(student);
            return;
        }
        throw new EntityNotFoundException("student, id:" + studentToEdit.getStudentId());
    }

    public void delete(Long id) {
        if (studentRepository.existsById(id)) {
            studentRepository.deleteById(id);
            return;
        }
        throw new EntityNotFoundException("student, id:" + id);
    }

    public Page<Student> getPage(PageRequest of) {
        return studentRepository.findAll(of);
    }

    public Long addGradeToStudent(AddGradeToStudent addGradeToStudent) {
        Optional<Student> studentOptional = studentRepository.findById(addGradeToStudent.getStudentId());
        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();

            Grade grade = gradeMapper.createGradeFromDto(addGradeToStudent);
            grade.setStudent(student);

            return gradeRepository.save(grade).getId();
        }
        throw new EntityNotFoundException("student, id:" + addGradeToStudent.getStudentId());
    }

    public Long assignGradeToStudent(AssignGradeToStudent dto) {
        Optional<Student> studentOptional = studentRepository.findById(dto.getStudentId());
        if (!studentOptional.isPresent()) {
            throw new EntityNotFoundException("student, id:" + dto.getStudentId());
        }
        Optional<Grade> gradeOptional = gradeRepository.findById(dto.getGradeId());
        if (!gradeOptional.isPresent()) {
            throw new EntityNotFoundException("grade, id:" + dto.getGradeId());
        }
        Student student = studentOptional.get();
        Grade grade = gradeOptional.get();

        if (grade.getStudent() != null) {
            throw new WrongOperation("You should not assign grade that is already assigned. Use /reassign mapping.");
        }
        grade.setStudent(student);

        return gradeRepository.save(grade).getId();
    }
}
