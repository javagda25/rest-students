package com.javagda25.restapi.service;

import com.javagda25.restapi.mapper.GradeMapper;
import com.javagda25.restapi.model.Grade;
import com.javagda25.restapi.model.dto.CreateGradeDto;
import com.javagda25.restapi.repository.GradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GradeService {
    @Autowired
    private GradeRepository gradeRepository;
    @Autowired
    private GradeMapper gradeMapper;

    public Long putGrade(CreateGradeDto dto) {
        Grade grade = gradeMapper.createGradeFromDto(dto);

        return gradeRepository.save(grade).getId();
    }
}
