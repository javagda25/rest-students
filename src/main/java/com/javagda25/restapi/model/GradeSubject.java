package com.javagda25.restapi.model;

public enum GradeSubject {
    ENGLISH,
    POLISH,
    MATH,
    IT,
    CHEMISTRY,
    PHYSICS,
    HISTORY,
    PE;
}