package com.javagda25.restapi.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssignGradeToStudent {
    private Long gradeId;
    private Long studentId;
}
