package com.javagda25.restapi.model.dto;

import com.javagda25.restapi.model.GradeSubject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// podajemy id studenta i wszystkie niezbędne parametry do utworzenia nowej oceny
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddGradeToStudent {
    private Long studentId;
//    private CreateGradeDto gradeDto;
    private double value;
    private GradeSubject subject;
}
