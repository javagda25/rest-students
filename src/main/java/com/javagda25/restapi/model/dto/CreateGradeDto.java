package com.javagda25.restapi.model.dto;

import com.javagda25.restapi.model.GradeSubject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


// podajemy id studenta i wszystkie niezbędne parametry do utworzenia nowej oceny
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateGradeDto {
    private Integer id;
    private double value;
    private GradeSubject subject;
}
