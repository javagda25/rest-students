package com.javagda25.restapi.mapper;

import com.javagda25.restapi.model.Grade;
import com.javagda25.restapi.model.Student;
import com.javagda25.restapi.model.dto.AddGradeToStudent;
import com.javagda25.restapi.model.dto.CreateGradeDto;
import com.javagda25.restapi.model.dto.CreateStudentRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface GradeMapper {

    Grade createGradeFromDto(CreateGradeDto dto);

    @Mappings(value = {
            @Mapping(source = "value", target = "value"),
            @Mapping(source = "subject", target = "subject"),
            @Mapping(target = "id", ignore = true)
    })
    Grade createGradeFromDto(AddGradeToStudent dto);
}
