package com.javagda25.restapi.controller;

import com.javagda25.restapi.model.dto.CreateGradeDto;
import com.javagda25.restapi.service.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/grade")
public class GradeController {
    @Autowired
    private GradeService gradeService;

    @PutMapping
    public Long putGrade(@RequestBody CreateGradeDto dto){
        return gradeService.putGrade(dto);
    }


}
