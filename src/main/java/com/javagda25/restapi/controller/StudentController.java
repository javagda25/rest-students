package com.javagda25.restapi.controller;

import com.javagda25.restapi.model.Student;
import com.javagda25.restapi.model.dto.AddGradeToStudent;
import com.javagda25.restapi.model.dto.AssignGradeToStudent;
import com.javagda25.restapi.model.dto.CreateStudentRequest;
import com.javagda25.restapi.model.dto.StudentUpdateRequest;
import com.javagda25.restapi.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/student")
public class StudentController {

    // C   R    U    D
    // PUT GET  POST DELETE

    @Autowired
    private StudentService studentService;

    @GetMapping
    public List<Student> getStudentList() {
        return studentService.getAll();
    }

    @GetMapping("/{id}")
    public Student getById(@PathVariable("id") Long studentId) {
        return studentService.getById(studentId);
    }

    @PutMapping
    public Long putStudent(CreateStudentRequest student) {
        return studentService.save(student);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public void postStudent(StudentUpdateRequest student) {
        studentService.update(student);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public void delete(@PathVariable("id") Long id) {
        studentService.delete(id);
    }

    @GetMapping("/getPage")
    public Page<Student> getPage(@RequestParam(name = "pageNumber", defaultValue = "0") int page,
                                 @RequestParam(name = "pageSize", defaultValue = "5") int size) {
        return studentService.getPage(PageRequest.of(page, size));
    }

    @PostMapping("/grade")
    public Long addGrade(AddGradeToStudent addGradeToStudent) {
        return studentService.addGradeToStudent(addGradeToStudent);
    }

    @PostMapping("/assignGrade")
    public Long addGrade(AssignGradeToStudent dto) {
        return studentService.assignGradeToStudent(dto);
    }

//    @PostMapping("/reassign")
//    public Long addGrade(AssignGradeToStudent dto) {
//        return studentService.assignGradeToStudent(dto);
//    }
}
