package com.javagda25.restapi.exception;

public class WrongOperation extends RuntimeException {
    public WrongOperation(String message) {
        super(message);
    }
}
